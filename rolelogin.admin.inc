<?php


  function sessionLogoutOthers() {
    global $user;
    // see if user is logged-in on multiple locations
    $result_session = db_select('sessions', 's')
            ->fields('s')
            ->condition('uid', $user->uid,'=')
            ->execute()
            ->fetchAll();

    //echo "<pre>";print_r($result_session);echo "</pre>";
    if(count($result_session)>1) {
      // Delete Other Sessions From Table
      $num_updated = db_delete('sessions') // Table name no longer needs {}
        ->condition('uid', $user->uid, '=')
        ->condition('sid', session_id(), '!=')
        ->execute();       
      //$session_count = $result_session->rowCount();
    }
		$ret_data['success'] = 1;
		$ret_data['redirect'] = 1;
		return drupal_json_output($ret_data);
  }


	function modifyUserRole() {
    global $user;
		// Put Logic to modify user name.
    // rlid, uid, email, roles, activerole, interrupted
    // Initializing $record array with values.
    //echo "<pre>";print_r($user);      print_r($_REQUEST);    echo "</pre>";
    $roleid = '';
    if(isset($_POST['roleid'])) {
      global $user;
      $ret_data = array('redirect' => '');
      $ajax_req_data = $_POST;
      $roleid = $ajax_req_data['roleid'];
      
      // LP: Cross Check if Requested Role Exists in db table rolelogin correponding to UID.
      // AND  
      // Get All the Roles From Role Login Table
      $result = db_select('rolelogin', 'rl')
        ->fields('rl')
        ->condition('uid', $user->uid,'=')
        ->execute()
        ->fetchObject();        
      
      $all_roles = @unserialize($result->roles);
      
      if(array_key_exists($roleid, $all_roles) && $roleid != '2') {
        // Update User's Role Now in Users table & udpate the active role in rolelogin table.
        $mod_role = array('2' => 'authenticated user',$roleid => $all_roles[$roleid]);
        $user_data = user_load($user->uid);
        $user_data->roles = $mod_role;
        user_save($user_data);
        
			  // Update the Active Role
		    $num_updated = db_update('rolelogin') // Table name no longer needs {}
		      ->fields(array(
		        'activerole' => $roleid,
		      ))
		      ->condition('uid', $user->uid, '=')
		      ->execute();        
        
        // Destroy Session
        // Login again programatically

        // Redirect as per rules
        $ret_data['success'] = TRUE;
        $ret_data['redirect'] = '';
        return drupal_json_output($ret_data);
      }
      else {
        // Return Error
        return drupal_json_output(array('success' => FALSE, 'error' => 'You Have Not Assigned Such Role At All! Please Inform Your Web Guy Immediately.'));
      }
    }
		else {
			return drupal_json_output(array('success' => FALSE, 'error' => 'Woops, You Are Calling Me The Way I can\'t Reply!'));
		}
	}
?>
