<?php

/**
 * Callback function for admin/config/people/fancy_login
 * Allows the user to set various CSS settings related to the display of the popup window
 */
function rolelogin_settings($form, &$form_state) {
	$form['rolelogin'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('Popup Messages Sections'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['rolelogin']['role_explanation'] = array
	(
		'#value' => '<p>' . t('Show Appropriate Messages on header & footer of popup.') . '</p>',
	);
	$form['rolelogin']['role_header_msg'] = array
	(
	 	'#title' => t('Welcome Message'),
		'#type' => 'textarea',
		'#default_value' => variable_get('role_header_msg', 'white'),
		'#description' => t('Message to show to user, when they will login having multiple Roles.'),
	);  
	$form['rolelogin']['role_footer_msg'] = array
	(
	 	'#title' => t('Rolo Login Footer Message'),
		'#type' => 'textarea',
		'#default_value' => variable_get('role_footer_msg', ''),
		'#description' => t('Will Be Displayed At Bottom Of Role Login Popup Itself.'),
	);
	return system_settings_form($form);
}