
Why Swithc Role?
This module has been created under the impression that, if on one site user does have multiple roles. Its hard for him to understand the system.
So this Switch Role Module Show a small menu at the top with all the roles current logged in user does have, & he/she can select the Role from drop down & based on user role he can use the site.

Installation
------------

Copy rolelogin.module to your module directory and then enable on the admin
modules page.

Author
------
Arun Verma
av@fabwebstudio.com
