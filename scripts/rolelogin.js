// $Id: fancy_login.js,v 1.19 2011/01/26 15:31:23 hakulicious Exp $
(function ($) {
    var popupVisible = false;
    var ctrlPressed = false;

    function showLogin($message_number) {
      var settings = Drupal.settings.roleLogin;

        var loginBox = $("#fancy_login_login_box");
        if (!popupVisible) {
            popupVisible = true;
            if (settings.hideObjects) {
                $("object, embed").css("visibility", "hidden");
            }
            $("#fancy_login_dim_screen").css({
                "position": "fixed",
                "top": "0",
                "left": "0",
                "height": "100%",
                "width": "100%",
                "display": "block",
                "background-color": settings.screenFadeColor,
                "z-index": settings.screenFadeZIndex,
                "opacity": "0"
            }).fadeTo(settings.dimFadeSpeed, 0.8, function () {
                loginBox.css({
                    "position": "fixed",
                    "width": settings.loginBoxWidth,
                    "height": settings.loginBoxHeight
                });
                var wHeight = window.innerHeight ? window.innerHeight : $(window).height();
                var wWidth = $(window).width();
                var eHeight = loginBox.height();
                var eWidth = loginBox.width();
                var eTop = (wHeight - eHeight) / 2;
                var eLeft = (wWidth - eWidth) / 2;
                if ($("#fancy_login_close_button").css("display") === "none") {
                    $("#fancy_login_close_button").css("display", "inline");
                }
                loginBox.css({
                    "top": eTop,
                    "left": eLeft,
                    "color": settings.loginBoxTextColor,
                    "background-color": settings.loginBoxBackgroundColor,
                    "border-style": settings.loginBoxBorderStyle,
                    "border-color": settings.loginBoxBorderColor,
                    "border-width": settings.loginBoxBorderWidth,
                    "z-index": (settings.screenFadeZIndex + 1),
                    "display": "none",
                    "padding-left": "15px",
                    "padding-right": "15px"
                })
                    .fadeIn(settings.boxFadeSpeed);
                loginBox.find(".form-text:first").focus().select();
                setCloseListener();
            });
        }
    }

    function setCloseListener() {
        $("#fancy_login_dim_screen, #fancy_login_close_button").click(function () {
            hideLogin();
            return false;
        });
        $("#fancy_login_login_box form").submit(function () {
            submitted();
        });
        $("#fancy_login_login_box a:not('#fancy_login_close_button')").click(function () {
            submitted();
        });
        $(document).keyup(function (event) {
            if (event.keyCode === 27) {
                hideLogin();
            }
        });
    }

    /*
     * Login User With Ajax.
     **/
    function modify_user_role (s_role) {
        var settings = Drupal.settings;
        
        // Put Data to be sent in an string  pass it.
        var form_values =  "roleid="+s_role;//+"&all_roles=" + Drupal.settings.all_roles;

        //console.log("Prod ID: "+Drupal.settings.product_id);
        // Run Ajax to login user & add product to cart itself or show error message if exists.
        $.ajax({
            url: settings.basePath + '?q=rolelogin/role',
            type: 'POST',
            data: form_values,
            dataType: 'json',
            success: function (data) {
              if (data) {
								console.log("If You Smell What The Rock Is Cooking!");
              }
              else {
								console.log("Rock is Not Cooking.");
              }
            }
        });
    }

    Drupal.behaviors.roleLogin = {
      attach: function () {
        var settings = Drupal.settings.roleLogin;
        if (!$.browser.msie || $.browser.version > 6 || window.XMLHttpRequest) {
        	
        	/*console.log(settings);
        	$temp_menu = "<li class='rolelogin'><span>Switch Role</span><ul class='rolelogin-ul'>";
        	$.each(settings.custom_menu,function(id,value) {
        		$temp_menu += "<li><a class='role-switch' href='"+id+"'>"+value+"</a>";
        	});
        	$temp_menu += "</ul></li>";
        	$('#secondary-menu-links,#block-system-user-menu').append($temp_menu);
        	
        	$('#secondary-menu-links .role-switch,#block-system-user-menu .role-switch').live('click',function() {
        			modify_user_role($(this).attr('href'));
        			return false;
        	});
        	
        	if(settings.showpopup == "1") {
        		// Call Automatic Popup Here To Be Shown
        		alert("Popup Is Coming Soon in Next Release");
        	}
        	else {
        		//alert("What To Show Except Popup");
        	}
		  		$(".rolelogin-block a.role").each(function () {
            $(this).click(function () {
              modify_user_role($(this).attr('rel'));
              return false;
            });
          });*/
        }
      }
    };
}(jQuery));
